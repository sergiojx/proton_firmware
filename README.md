# proton_firmware
```
VERSIONINFO = {
  DEVICE_TYPE = "PROTON-16Ai+HART",
  FIRMWARE_VER = "1_4_rc6_1_l2900_ut_iot_io1",
  HARDWARE_CODE = "PROTON"
}
```
```
RELEASE_NOTES = {
  v1_4_rc6_1 = "28/09/23 Adding support for Config file V1.2, Adding suport for personal Hotspots (MAC, Android)",
  v1_4_rc6 = "10/09/23 Adding TLSv1.2 for SMTP email server",
  v1_4_rc5 = "25/08/23 Adding error control for DNS failures in IoT frames",
  v1_4_rc4 = "20/06/23 Upgrade Azure libs and Openssl_v3 libs",
  v1_3_rc7_1 = "19/03/20 Change DB query strategy for charts",
  v1_3_rc7 = "03/03/20 CSV download all data from view",
  v1_3_rc6 = "10/01/20 Fixed ID on Trends Database",
  v1_3_rc5 = "18/09/19 FTP with IZINET",
  v1_3_rc4 = "12/08/19 Finish Google IoT initila implementation. Remove -Copy- text from get/copy key button into te Google IoT configuration form",
  v1_3_rc4 = "08/08/19 Implement auxiliary Google IoT service. This executes IoT core client. Pending: monitor IoT core client, get published data",
  v1_3_rc3 = "31/07/19 Implemet Google IoT tx file directory and data file tree",
  v1_3_rc2 = "31/07/19 Add UIN64 and UINT64 basic support",
  v1_3_rc1 = "30/07/19 Implement Google IoT module",
  v1_2_rc12 = "27/06/19 Implement FTP uploader status logic and prefix logic",
  v1_2_rc12 = "25/06/19 Set default ppp serial driver baud-rate to 115200",
  v1_2_rc12 = "20/06/19 Implement FTP Uploader module",
  v1_2_rc12 = "20/06/19 fix UINT16 format hand writng clipping when data base web user interaction",
  v1_2_rc11 = "07/06/19 fix associated to error: webinterfacelib.lua:36. This time related with udhcpd and WLAN in AP mode",
  v1_2_rc10 = "20/05/19 implment Azure IoT Cloud To Device message strategy",
  v1_2_rc10 = "20/05/19 fix associated to error: webinterfacelib.lua:36: address already in use",
  v1_2_rc9 =  "17/05/19 Add IZINET unknown tx error crashlog logic",
  v1_2_rc8 =  "22/04/19 Add tx enable register to Azure IoT module",
  v1_2_rc7.1 =  "?",
  v1_2_rc7 =  "12/01/19 Adding Plotly as main charts library, changing IZI image for Proton image",
  v1_2_rc6 =  "11/12/18 fix luaSerial library",
  v1_2_rc5 =  "06/12/18 Adding control in IZINET900.lua when IZINET radio is not found ",
  v1_2_rc4 =  "30/11/18 fix base line root file system resolv.conf simbolink link",
  v1_2_rc3 =  "23/11/18 Enable Mobile pppd access user defined AT script, dhcp client is executed in background to prevent blocking",
  v1_2_rc2 =  "20/11/18 Add Trends and LiveView Scales logic",
  v1_2_rc1 =  "14/11/18 Add GSM gateway module",
  v1_1 =  "08/11/18 CHART module: remove pen scale when this is not selected, prevent more than one request at the same time",
  v1_1_0rc3 =  "06/11/18 Fix time-date format, Change/fix LiveView-Trend, Change ftp root",
  v1_1_0rc2 =  "29/10/18 Implement TRENDS data reading query auxliary process. Add sever error Js logic to LiveView and Trend web files",
  v1_1_0rc1 =  "08/10/18 Remove multi table scheme in the TRENDS module. Single DB-table per pen",
  v1_00_099 =  "24/08/18 Add Export-Import CSV for IZINET. Fix Import/Export CSV construction for AzureIoT",
  v1_00_098 =  "22/08/18 Add web logic for limiting TREND requested data qunatiy",
  v1_00_097i = "21/08/18 Become workingTable[DBRRQ_RXACK] a structure. This value is chcked(requested) from web",
  v1_00_097h = "16/08/18 Rreduce TRENDS WepRequest buffer de 4K. Add KillAuxiliarprocess function to main.lua",
  v1_00_097g = "14/08/18 Implement udhcp client",
  v1_00_097f = "10/08/18 Encrease Table size to 8K in TrendCHART. Increase luasocket(buffer.h.c) buffer size to from 24K 2 10K",
  v1_00_097e = "10/08/18 Implement axu. process table DROP/CREATE for TrendCHART. Increase luasocket(buffer.h.c) buffer size to from 10K 2 24K",
  v1_00_097d = "06/08/18 Implement axu. process for TrendCHART storage task. Increase luasocket(buffer.h.c) buffer size to from 8K 2 10K",
  v1_00_097c = "26/07/18 Implement AzureIoT multy thing",
  v1_00_097b = "23/07/18 Add timeout flags to e-mail with attachment with curl",
  v1_00_097a = "16/07/18 Encrease Azure IoT TX buff. Add Count field to LOgDisk commad. Add Export/Import to Azure IoT",
  v1_00_097 = "11/07/18 Change LOGDISK logic, implement AZURE IoT TX Groups",
  v1_00_096a = "29/06/18 Ignore AB08X logic for I2C RTC",
  v1_00_096 = "27/06/18 Change SysClok wierd change detection logic. Change AzureIoT Tx Scheme. Add SysClock Update into AzureIoT logic" ..
              "LogDisk generated files are stored and/or sent as .gz file",
  v1_00_095d = "23/06/18 change ls -lrt in MICROSD",
  v1_00_095c = "22/06/18 implement uSD room monitor into MICROSD module. add delays in attachmentemail",
  v1_00_095b = "21/06/18 dhcp client on development, mode email .gz from uS to tmp, fix trend request with offset",
  v1_00_094 = "20/06/18 add udhcp client. Add time zon offset to trendcharts. email script Tx 3 times",
  v1_00_093 = "Fix logdisk bug when config table does not exist",
  v1_00_092 = "15/06/18 set io1 fro classic combo configuration",
  v1_00_091 = "14/06/18 share LOGDISK config table with ATTACHEMAIL",
  v1_00_090 = "email with attachment with STARTTLS support",
  v1_00_089 = "Fix resolv.conf.Add base64",
  v1_00_088 = "Add trends/LiveView user url, change logdisk name format, remove IZINET coroutine.yield() during config reading",
  v1_00_087 = "httpd.config changed. Now hmiview user has acces to status",
  v1_00_086 = "Add serial script functions luaserial, uadebugprint.lua and scriptsutil.lua",
  v1_00_085 = "LogDisk and e-mail service coupling, attachEmailClient.lastLogDiskFile()",
  v1_00_084 = "Add send email with attachment",
  v1_00_083 = "AZURE IoT Storage&Forward, time zone offset(LogDisk&AzureIoT), txqueue 4 Azure ioT",
  v1_00_082 = "14/05/18 Erroneous systime change detect, ftp set up, logdisk biary upload partial",
  v1_00_081 = "10/05/18 Add periodic sys time udate from RTC ref.",
  v1_00_080 = "09/05/18 Add logDISK, add gzip and proftpd",
  v1_00_079 = "02/05/18 trend request time zoe gnostic",
  v1_00_078 = "22/02/18 Add warning message and change start AI index from 0 to 1 for PRE02",
  v1_00_077 = "04/02/18 Add scaling web controls and lua logic to 10Ai 6Di hardware setup",
  v1_00_076 = "31/01/18 parseFloat in trends and liveView interpolation lines. Add warning when change EXTXHART.js trend pen label",
  v1_00_075 = "16/01/18 Split off versioninfo table",
  v1_00_074 = "22/12/17 New firmaware update scheme",
  v1_00_072 = "11/12/17 Fix izid root=ubi0: designation",
  v1_00_071 = "04/12/17 Add IO2 scaling logic",
  v1_00_070 = "22/11/17 Fix io2 board javaScript bug",
  v1_00_068 = "28/10/17 AZUREIOT.lua. add timeStamp, update C library",
  v1_00_067 = "17/10/17 MACSRVC.lua. Firt check PLATFORM.ETH0MAC_DIR existence",
  v1_00_066 = "09/10/17 IZINET900.lua. Add radio configuration retry, when MAC is not picked up",
  v1_00_065 = "26/09/17 Add Iot modules,remove charts from status, other fixes. email and HMI update",
  v1_00_064 = "01/08/17 Add SMS message tx from user script",
  v1_00_063 = "01/08/17 adding and testing Azure IoT module set",
  v1_00_062 = "28/07/17 add conx module for IZICLOUD",
  v1_00_061 = "27/07/17 adding and testing Azure IoT module set",
  v1_00_06 = "05/07/17 add indirect access to alarm/event module. This is done trough config. dialog",
  v1_00_05 = "04/07/17 change swap 2 logic in luabinbuffer.c setFloat, setINT32 and setUINT32",
  v1_00_05 = "04/07/17 interchange swap options 1 and 3 in in luabinbuffer.c setFloat, setINT32 and setUINT32",
  v1_00_05 = "04/07/17 fix wlan scaning, authentication and ecryption auto fill feature",
  v1_00_05 = "04/07/17 increase amount of allowed pens in trend and real view js module",
  v1_00_05 = "04/07/17 fix default swap (0) logic in luabinbuffer.c getFloat function",
  v1_00_04 = "08/06/17 Update HMI moules",
  v1_00_03 = "07/06/17 Add alarm and event log module",
  v1_00_02 = "18/05/17 Add email feature",
  v1_00_00 = "15/05/17 Add gateway logic, fix HMI aliases feature"
}
```
# X.Y.Z Version Coding Scheme

In this coding version scheme three digits are used for version coding. Most significant digit will change
in order to reflect important modifications. These can involve some backward incompatibilities. Versions 
that are coded with the pattern 0.Y.Z mean that implementation lives up to minimum requirements but it is
not fully functional. Each change to the most significant digit can involve tasks like code refactoring, 
module redesign or a bunch of new feature additions.

The second digit X.0.Z should be changed when minor functionalities are added or fixed. This involve minor
bug fixing or minor functionalities additions that keep backward compatibilities.

Finally third digit X.Y.0 is changed when performed changes won't be noted by user. These are modifications
that don't change user interaction with the application.

It is expected that a change in a particular digit will conduct right side digit initialization to zero. 
When not fully teste versions are released, it is common to add a letter to the right end. For example
if the current system version is 1.9.0, and before releasing a version 2.0 with a bunch of new features
, this version must be tested on field, this software could be released as version 1.9.0a or 2.0a. If 
during field testing some other changes are added, these versions can be named 1.9.0b or 2.0b. Another 
approach makes use of the acronym RC which stands for Release Candidate. Using previous example, the version
coding would something like 2.0-RC1 and 2.0-RC2

## More Information Links
[Semantic Versioning 2.0.0](https://semver.org/)

[Semantic Versioning 2.0.0-rc.1](https://semver.org/spec/v2.0.0-rc.1.html)


## Proton Version Glosary
* l2900: Xbee 900MHz Radio at l2 Socket
* l1900: Xbee 900MHz Radio at l1 Socket
* l22.4: Xbee 2.4GHz Radio at l2 Socket
* l12.4: Xbee 2.4GHz Radio at l1 Socket
* l2SMS: SMS Feature MODEM at socket 2
* io01: IO Borad with 2 Digital Outpus, 4 Digital Inputs, 1 Analog Output, 6 Analog Inputs
* io02: IO Board with 16 Analog Inpus
* iot: Cloud Access
* ut: uSD and Trends
* 10Ai_6Di_io2: io02 IO Board with adaptations


